angular.module('dicoulti.donate', [])

  .config(function($stateProvider, $urlRouterProvider) {

    $stateProvider

    .state('dicoulti.donate', {
        url: '/donate',
        bodyClass: 'donatePage',
        views: {
          'menuContent': {
            templateUrl: 'templates/donate.html',
            controller: 'DonateCtrl'
          }
        }
    })
  })

  .controller('DonateCtrl', function($scope) {

  })
