angular.module('dicoulti.breats-cancer', [])

  .config(function($stateProvider, $urlRouterProvider) {

    $stateProvider

    .state('dicoulti.breats-cancer', {
        url: '/breats-cancer',
        bodyClass: 'breatsCancerPage',
        views: {
          'menuContent': {
            templateUrl: 'templates/breats-cancer.html',
            controller: 'BreatsCancerCtrl'
          }
        }
    })
  })

  .controller('BreatsCancerCtrl', function($scope) {

  })
