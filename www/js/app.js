// Dicoulti

angular.module('dicoulti', [
  'ionic',
  'ngCordova.plugins.network',
  'ionic.service.core',
  'ionic.service.analytics',
  'ionic.utils',
  'ngIOS9UIWebViewPatch',
  'dicoulti.rating',
  'dicoulti.wall',
  'dicoulti.breats-cancer',
  'dicoulti.login',
  'dicoulti.config',
  // 'dicoulti.donate',
  'dicoulti.top10',
  // 'dicoulti.upload',
  'dicoulti.settings'
])

.run(function($ionicPlatform, $ionicAnalytics, $rootScope, $window, $ionicModal, $state, FB_APP_ID, $auth) {

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)

    $ionicAnalytics.register();

    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    // var push = new Ionic.Push({
    //   "debug": true
    // });

    // push.register(function(token) {
    //   console.log("Device token:",token.token);
    // });

  });

  $rootScope.$on('$stateChangeStart', function(event, toState, $rootScope) {

      if (toState.name !== "login" && !$auth.isAuthenticated() ) {
          event.preventDefault();
          $state.go('login');
          console.info('notlogged');
      }

      if(toState.name == "login" && $auth.isAuthenticated() ){
        console.info('Logged');
        event.preventDefault();
        $state.go('dicoulti.rating');
      }

      $rootScope.bodyClass = toState.bodyClass;
  });

})

// ------------------------------------------
//   Routes
// ------------------------------------------
.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {

  $ionicConfigProvider.views.maxCache(1);

  $stateProvider
  .state('dicoulti', {
    url: '/dicoulti',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  });

  $urlRouterProvider.otherwise('/dicoulti/rating');

})

// ------------------------------------------
//   Controller
// ------------------------------------------
.controller('AppCtrl', function($scope, $window,$cordovaNetwork,$rootScope) {

  $scope.user = JSON.parse( $window.localStorage.user || '{}' )

  $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
    alert('your phone is offline'+ networkState);
  });

  $scope.$on('updateUserInfo', function(event) {
      $scope.user = JSON.parse( $window.localStorage.user || '{}' );
  });

})
