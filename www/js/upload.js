angular.module('dicoulti.upload', ['ngCordova'])

.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider

        .state('dicoulti.upload', {
        url: '/upload',
        bodyClass: 'uploadPage',
        views: {
            'menuContent': {
                templateUrl: 'templates/upload.html',
                controller: 'UploadCtrl'
            }
        }
    })
})

.controller('UploadCtrl', function($scope,  $cordovaCamera, $ionicLoading, $localstorage, $ionicActionSheet, $ionicModal) {

    $scope.data = {
        "ImageURI": "Select Image"
    };

    $ionicModal.fromTemplateUrl('confirmationModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modal = modal;
      });
      $scope.openModal = function() {
        $scope.modal.show();
      };
      $scope.closeModal = function() {
        $('#photo').attr('src','');
        $scope.modal.hide();
      };
      //Cleanup the modal when we're done with it!
      $scope.$on('$destroy', function() {
        $scope.modal.remove();
      });
      // Execute action on hide modal
      $scope.$on('modal.hidden', function() {
        // Execute action
      });
      // Execute action on remove modal
      $scope.$on('modal.removed', function() {
        // Execute action
      });


    var hideSheet;
    $scope.showMenu = function() {
       hideSheet = $ionicActionSheet.show({
         titleText: 'Select source',
         buttons: [
           { text: 'Camera' },
           { text: 'Library' }
         ],
         cancelText: 'Cancel',
         cancel: function() {
              console.log('cancel');
          },
         buttonClicked: function(index) {
            
            if (index === 0 ) {
                return takePicture();
            }else if( index === 1 ){
                return selectPicture();
            }
            hideSheet();
         }
       });
    };

    takePicture = function() {
        hideSheet();
        var options = {
            quality: 80,
            destinationType: Camera.DestinationType.FILE_URL,
            sourceType: Camera.PictureSourceType.CAMERA
        };

        $cordovaCamera.getPicture(options).then(
            function(imageData) {
                $ionicLoading.show({
                    template: 'In progress...',
                    duration: 800
                });
                $scope.picData = imageData;
                $scope.ftLoad = true;
                $localstorage.set('fotoUp', imageData);
                $scope.openModal();
                $('#photo').attr('src',imageData);
            },
            function(err) {
                $ionicLoading.show({
                    template: 'Error :/',
                    duration: 800
                });
            }
        )
    }

    selectPicture = function() {
        hideSheet();
        var options = {
            quality: 80,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY
        };

        $cordovaCamera.getPicture(options).then(
            function(imageURI) {
                $ionicLoading.show({
                    template: 'In progress...',
                    duration: 800
                });
                $scope.openModal();
                $('#photo').attr('src',imageURI);
                window.resolveLocalFileSystemURI(imageURI, function(fileEntry) {
                    console.log('3');
                    $scope.picData = fileEntry.nativeURL;
                    console.log('4');
                    $scope.ftLoad = true;
                    var image = document.getElementById('photo');
                    image.src = fileEntry.nativeURL;
                });
                //$('#photo').attr('src',fileEntry.nativeURL);
            },
            function(err) {
                $ionicLoading.show({
                    template: 'Error...',
                    duration: 800
                });
            })
    };

    var uploadPicture = function() {
        $ionicLoading.show({
            template: 'Uploading, please wait...'
        });
        var fileURL = $scope.picData;
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";
        options.chunkedMode = true;

        var params = {};
        params.value1 = "someparams";
        params.value2 = "otherparams";

        options.params = params;

        var ft = new FileTransfer();
        ft.upload(fileURL, encodeURI("http://www.yourdomain.com/upload.php"), viewUploadedPictures, function(error) {
            $ionicLoading.show({
                template: 'Error... '+ error
            });
            $ionicLoading.hide();
        }, options);
    }

    // var viewUploadedPictures = function() {
    //     $ionicLoading.show({
    //         template: 'get the images...'
    //     });
    //     server = "http://www.yourdomain.com/upload.php";
    //     if (server) {
    //         var xmlhttp = new XMLHttpRequest();
    //         xmlhttp.onreadystatechange = function() {
    //             if (xmlhttp.readyState === 4) {
    //                 if (xmlhttp.status === 200) {
    //                     document.getElementById('server_images').innerHTML = xmlhttp.responseText;
    //                 } else {
    //                     $ionicLoading.show({
    //                         template: 'Errore durante il caricamento...',
    //                         duration: 1000
    //                     });
    //                     return false;
    //                 }
    //             }
    //         };
    //         xmlhttp.open("GET", server, true);
    //         xmlhttp.send()
    //     };
    //     $ionicLoading.hide();
    // }

    // $scope.viewPictures = function() {
    //     $ionicLoading.show({
    //         template: 'Sto cercando le tue foto...'
    //     });
    //     server = "http://www.yourdomain.com/upload.php";
    //     if (server) {
    //         var xmlhttp = new XMLHttpRequest();
    //         xmlhttp.onreadystatechange = function() {
    //             if (xmlhttp.readyState === 4) {
    //                 if (xmlhttp.status === 200) {
    //                     document.getElementById('server_images').innerHTML = xmlhttp.responseText;
    //                 } else {
    //                     $ionicLoading.show({
    //                         template: 'Errore durante il caricamento...',
    //                         duration: 1000
    //                     });
    //                     return false;
    //                 }
    //             }
    //         };
    //         xmlhttp.open("GET", server, true);
    //         xmlhttp.send()
    //     };
    //     $ionicLoading.hide();
    // }
})
