angular.module('ionic.utils', ['dicoulti.config'])

.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}' );
    }
  }
}])

.factory('boobs', function ($http, $rootScope, $stateParams, SERVER_PATH) {
  return {
    all: function () {
      return $http.get(SERVER_PATH + '/getPhotos')
    },
    page: function(page){
      return $http.get(SERVER_PATH + '/getPhotos?page=' + page)
    },
    rated: function () {
      return $http.get(SERVER_PATH + '/getRatedPhotos')
    },
    ratedPage: function (page) {
      return $http.get(SERVER_PATH + '/getRatedPhotos?page=' + page)
    },
    top10: function () {
      return $http.get(SERVER_PATH + '/getTopPhotos')
    }
  };
});
