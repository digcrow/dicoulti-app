angular.module('dicoulti.settings', [])

  .config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('dicoulti.settings', {
        url: '/settings',
        bodyClass: 'settingsPage',
        views: {
          'menuContent': {
            templateUrl: 'templates/settings.html',
            controller: 'SettingsCtrl'
          }
        }
    });
  })

  .controller('SettingsCtrl', function($scope, $state, $window, $auth, $ionicModal) {
    
    $scope.fbLogout = function () {
      facebookConnectPlugin.logout(
        function (response) {
          $auth.logout();
          $window.localStorage.clear();
          $state.go('login');
        },
        function (response) {
          console.log(response);
        });
    };

    $ionicModal.fromTemplateUrl('templates/login-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.openModal = function(content) {
        var ctn = {};
        ctn.title = content;
        ctn.content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure nemo vel dolorum odit praesentium ab est alias placeat, eum quasi molestias non laudantium magni quam voluptatibus. Aliquam animi, laborum quibusdam quidem consequuntur placeat, ullam! Rem molestiae placeat debitis, reiciendis accusamus possimus ducimus eius magni! Nihil esse inventore, et doloribus adipisci sequi quas similique quos dicta, provident beatae. Saepe, quam vero sint reiciendis doloremque esse hic possimus quis. Maiores tempora, voluptate excepturi quidem eveniet ipsum a autem unde consequatur sit! Voluptatibus asperiores perferendis consequuntur hic, ratione ipsa error labore optio tempora reiciendis, magnam ex perspiciatis, officia quae possimus fuga. Eveniet repellat aut consequuntur voluptate iste, recusandae iusto. Placeat at pariatur saepe doloribus esse aperiam quia maiores amet ut, iste repellendus accusamus alias quam labore adipisci quos. Itaque ipsum quibusdam soluta praesentium, aspernatur, ab amet, minus eum accusamus similique minima architecto nemo perferendis. Repellendus ipsum doloremque dolore, iste non aliquid a veniam maiores tempore totam, repudiandae in inventore nulla magni molestiae cum sapiente rem odio quod, voluptate? Autem voluptate magni eius odit, ex quas molestiae quae quam, hic sed ullam iste omnis facere inventore sunt in numquam laborum tenetur odio, ab, reiciendis fugit rem et. Minus sapiente odit eaque, suscipit omnis aut architecto magni eos ducimus doloribus ratione, debitis maxime facilis dolorem soluta voluptates impedit necessitatibus voluptatum. Non quasi vel nemo saepe fugiat laudantium culpa expedita cupiditate! Reiciendis voluptatem dolorum tempore magni laboriosam nam totam magnam fuga facere perspiciatis ea, at perferendis, nesciunt sint cum quia tenetur veritatis soluta saepe mollitia consequuntur. Iste perferendis nulla, accusantium sunt corporis, cupiditate mollitia facere iure eum itaque, vero quisquam reiciendis iusto accusamus explicabo corrupti rem tempore deleniti libero vel inventore nobis numquam! Rem voluptatum delectus aspernatur nam commodi quam tenetur quod exercitationem! Doloremque eos sapiente repudiandae sit error, animi aspernatur temporibus, dolor veritatis, placeat debitis amet sunt, atque delectus nihil provident enim soluta et ratione qui unde. Maiores fuga quam, nesciunt recusandae numquam. Possimus cumque dignissimos illo! Deserunt reprehenderit asperiores eos, at non numquam libero delectus hic illo est in sapiente incidunt veniam atque neque reiciendis sed ullam ipsa dignissimos doloremque! Reiciendis aliquam soluta eius iste sequi, nisi officiis quisquam eaque laudantium, quo consectetur, eligendi sint voluptatum dolorum rem, dignissimos aut vitae accusamus dolore. Nulla et saepe expedita, tempore, dolores harum dignissimos sint molestias maxime. Reprehenderit non iste praesentium hic at obcaecati sed doloremque perferendis. Maiores harum rem dolorum dolor eveniet, nobis. Repellat perspiciatis, voluptatibus.';
        $scope.content = ctn;
        $scope.modal.show();
        return false;
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
        var ctn = {};
        ctn.title = '';
        ctn.content = '';
        $scope.content = ctn;
        return false;
    };
    
  });
