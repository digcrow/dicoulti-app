angular.module('dicoulti.rating', [])

.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('dicoulti.rating', {
        url: '/rating',
        bodyClass: 'ratingPage',
        views: {
            'menuContent': {
                templateUrl: 'templates/rating.html',
                controller: 'RatingCtrl'
            }
        }
    })
})

.controller('RatingCtrl', function($scope, boobs, $http, SERVER_PATH, $window, $ionicLoading, $auth, $state) {
    if (window.AdMob)
        AdMob.showInterstitial();

    $ionicLoading.show({
        template: 'Boobs are coming, Please wait...'
    });

    var displayedBoob = 0, currentPage = 0;
    boobs.all().success(function(response) {

        $ionicLoading.hide();

        if (!response.data.length) {
            $('.boob-last').css('display', 'block').addClass('zoomIn');
        }

        console.info('First Loaded Photos ->' + response );

        currentPage     = response.current_page;
        lastPage        = response.last_page;
        displayedBoob   = toRate = response.per_page;

        var temp = [];
        angular.forEach(response.data, function(value, key) {
            value.pic = SERVER_PATH + '/getPhoto/' + value.img;
            this.push(value);
        }, temp);

        $scope.boobs = temp;
    }).error(
        function(response){
            $ionicLoading.hide();
            if (response.error == 'token_invalid') {
                $auth.logout();
                $window.localStorage.clear();
                $state.go('login');
            }
        }
    );

    var showing = 1;

    function nextPage (){

    
        AdMob.showInterstitial();
        showing = 0;
        showing ++;


        if (lastPage == currentPage) {
            return false;
        }

        $ionicLoading.show({
            template: 'Boobs are coming, Please wait...'
        });

        boobs.page( currentPage + 1 ).success(function(response) {
            $ionicLoading.hide();

            if (!response.data.length) {
                $('.boob-last').css('display', 'block').addClass('zoomIn');
            }

            $('.boobs-carousel .boob').removeClass('zoomOutRight zoomOutDown zoomOutLeft');

            // nextPage    = response.next_page_url;
            currentPage = response.current_page;
            lastPage    = response.last_page;

            var temp = [];
            angular.forEach(response.data, function(value, key) {
                value.pic = SERVER_PATH + '/getPhoto/' + value.img;
                this.push(value);
            }, temp);
            
            // var temp = [];
            // angular.forEach(response.data, function(value, key) {
            //     $http.get(SERVER_PATH + '/getPhoto/' + value.img ).then(function successCallback(response) {
            //         value.pic = response.data;
            //     });
            //     this.push(value);
                
            // }, temp);

            $scope.boobs = temp;
        }).error(
            function(response){
                $ionicLoading.hide();
                if (response.error == 'token_invalid') {
                    $auth.logout();
                    $window.localStorage.clear();
                    $state.go('login');
                }
            }
        );
    }


    /*----------------------------------
        Rating Function
    ------------------------------------*/
    // Init Vars
    var ready         = true;
    var voteSubmitted = true;
    var toRate        = displayedBoob;

    $scope.rateMe = function(i,id,score) {

        if (!ready  || !voteSubmitted) {
            return;
        };

        voteSubmitted  = false;

        console.info(toRate, displayedBoob);

        $('#rateForm .btn').css('opacity',0);
        var lastCard = this.$last;

        $http.post(SERVER_PATH + '/vote/' + id, {rating: score}).success(function(data){
            $('#rateForm .btn').css('opacity',1);
            voteSubmitted = true;
            toRate -= 1;
            moveToTheNext(i, score, lastCard);
        }).error(function(error){
            console.log(error);
            $('#rateForm .btn').css('opacity',1);
            if (error.error == 'token_invalid') {
                $auth.logout();
                $window.localStorage.clear();
                $state.go('login');
            }
        });
    }


    // Animate the cards
    function moveToTheNext( current , direction, last){

        var next = current + 1
        if (!ready) {
            return;
        };

        ready = false;



        if (direction == 3) {
            $('.boob' + current).removeClass('zoomIn').addClass('zoomOutRight');
        }else if (direction == 2){
            $('.boob' + current).removeClass('zoomIn').addClass('zoomOutDown');
        }else if (direction == 1){
            $('.boob' + current).removeClass('zoomIn').addClass('zoomOutLeft');
        }

        setTimeout(function() {
            $('.boob' + current).fadeOut(50);
            ready = true;

            if (last) {
                toRate += displayedBoob;
                nextPage();
            }
        }, 700);

        setTimeout(function() {
           $('.boob' + next).css('display', 'block').addClass('zoomIn');
        }, 300);

    }

    $scope.closeTutos = function (){
        $('#tutos').fadeOut('500');
        $window.localStorage['alreadyExists'] = true;
    }

    if ( $window.localStorage['alreadyExists'] == 'true' ){
        $('#tutos').css('display','none');
    };
    
})
