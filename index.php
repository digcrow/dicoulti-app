<?php

$request = new HttpRequest();
$request->setUrl('https://api.twitter.com/1.1/search/tweets.json');
$request->setMethod(HTTP_METH_GET);

$request->setQueryData(array(
  'q' => '#react',
  'result_type' => 'recent',
  'count' => '10'
));

$request->setHeaders(array(
  'postman-token' => '3a3b045b-4ad7-0ad4-6a01-1ba807d8dc93',
  'cache-control' => 'no-cache',
  'authorization' => 'Bearer AAAAAAAAAAAAAAAAAAAAAC2E0QAAAAAAX9q%2BbkP0PgEVwlfbkiF7qoMooRI%3DIDsfrGNxMjEbYomNKIsOfmkd3bCJ7hu1muj5yrbKycZxqeKh29',
  'content-type' => 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
));

$request->setBody('------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="grant_type"

client_credentials
------WebKitFormBoundary7MA4YWxkTrZu0gW--');

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}
?>